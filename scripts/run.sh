#!/bin/bash

#source sctipts/version.sh

export NODE_OPTIONS='--max_old_space_size=2048'

echo
echo "Which platform?"
echo 1 Web [default]
echo 2 iOS
echo 3 Android
echo ---
echo 4 iOS Simulator
echo 5 Android Simulator
echo
read platform

if [ -z $platform ] ; then
	platform=1
fi

if [ $platform -eq "1" ] ; then
	platform=""
	platform_name=WEB

elif [ $platform -eq "2" ] ; then
#	cp platforms_ios .meteor/platforms
	platform=ios-device
	platform_name=iOS-Device
	buildPlatform="ios"
	buildEnvironment="dev"

elif [ $platform -eq "3" ] ; then
#	cp platforms_android .meteor/platforms
	platform=android-device
	platform_name=Android-Device
	buildPlatform="android"
	buildEnvironment="dev"
	cd ~/git/apkTest

	meteor remove-platform ios
#	meteor remove-platform android
	meteor add-platform android
	meteor reset
	
	echo ///////////////////////////////////
	adb devices
	# if no devices, then do:
	#	adb  kill-server
	#	adb start-server
	echo ///////////////////////////////////

elif [ $platform -eq "4" ] ; then
#	cp platforms_ios .meteor/platforms
	platform=ios
	platform_name=iOS-Simulator
	buildPlatform="ios"
	buildEnvironment="dev"

elif [ $platform -eq "5" ] ; then
#	cp platforms_android .meteor/platforms
	platform=android
	platform_name=Android-Simulator
	buildPlatform="android"
	buildEnvironment="dev"
	
	meteor remove-platform android
	meteor add-platform android
	meteor reset
	
	echo ///////////////////////////////////
	adb devices
	# if no devices, then do:
	#	adb  kill-server
	#	adb start-server
	echo ///////////////////////////////////
fi

echo "Starting run now..."
date

meteor npm install --save

# echo
# echo "Which database would?"
# echo 1 "dev"
# echo 2 "test"
# echo 3 "prod"
# echo
# read environment

# if [ -z $environment ] ; then
# environment=1
# fi

# mongoURL="mongodb://spoticledb_user:gluino834@aws-eu-west-1-portal.9.dblayer.com:17244,aws-eu-west-1-portal.8.dblayer.com:17244/spoticleTest?ssl=true"

# if [ $environment -eq "1" ] ; then
# settingsFile=settings-dev.json
# buildEnvironment="dev"
# elif [ $environment -eq "2" ] ; then
# settingsFile=settings-test.json
# buildEnvironment="test"
# elif [ $environment -eq "3" ] ; then
# settingsFile=settings-prod.json
# buildEnvironment="prod"
# fi

settingsFile=settings-dev.json
mongoURL="mongodb://spoticledb_user:gluino834@aws-eu-west-1-portal.9.dblayer.com:17244,aws-eu-west-1-portal.8.dblayer.com:17244/spoticleTest?ssl=true"

#cp scripts/google-services.json .meteor/local/cordova-build/platforms/android


echo ///////////////////////////////////
echo RUNNING APK TEST ON $platform_name
echo version = $version
echo settings = $settingsFile
echo database = testDB
if  [ $buildPlatform ] ; then
	echo environment = $buildPlatform
fi
echo ///////////////////////////////////
echo 

BUILD_ENVIRONMENT=$buildEnvironment BUILD_PLATFORM=$buildPlatform MONGO_URL=$mongoURL meteor run $platform --mobile-server 192.168.178.26:3000 -s $settingsFile

