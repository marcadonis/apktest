#!/bin/bash

cd ~/git/apkTest

SECONDS=0

export BUILD_PLATFORM=android
export BUILD_ENVIRONMENT=test

settingsFile=settings-test.json
server=https://test.spoticle.com

echo building with settings file $settingsFile

# if we don't so this, the build randomly chooses to set the server to the previous value
rm -r ~/apkTest-build
rm -r .meteor/local/cordova-build

#cp platforms_android .meteor/platforms

meteor remove-platform android
meteor reset
meteor add-platform android

meteor npm install --save

#cp scripts/build.gradle .meteor/local/cordova-build/platforms/android
#cp scripts/google-services.json .meteor/local/cordova-build/platforms/android

meteor build --mobile-settings $settingsFile --verbose ~/apkTest-build --server=$server

echo 
echo "*******************************************"
echo "*** BUILD FINISHED IN $SECONDS SECONDS! ***"
echo "*******************************************"
echo 

