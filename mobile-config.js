var version = "1.4.1";
console.log("*** setting version ", version);

var buildNumber = Math.floor(+new Date() / (24 * 60 * 60));
console.log("*** setting buildNumber ", buildNumber);


console.log("*** configuring cc.fovea.cordova.openwith...");

App.setPreference("android-targetSdkVersion", "26");
App.setPreference("android-minSdkVersion", "19");
//App.setPreference("android-versionCode", versionCode);  // inexplicably ignored!

App.accessRule("*.google.com/*");
App.accessRule("*.googleapis.com/*");
App.accessRule("*.gstatic.com/*");
App.accessRule("http://*");
App.accessRule("https://*");
App.accessRule("https://spoticle001.s3-eu-west-1.amazonaws.com/*");
App.accessRule("*.amazonaws.com/*");
App.accessRule("https://spoticle.freshdesk.com/*", {
	type : "intent"
});
//App.accessRule("https://gravatar.com/*", {
//	type : "intent"
//});
//App.accessRule("https://*.gravatar.com/*", {
//	type : "intent"
//});

App.accessRule("http://server.spoticle.com/*");
App.accessRule("https://server.spoticle.com/*");

App.accessRule("https://appstore.com/spoticle", {
	type : "intent"
});

App.accessRule("https://creativecommons.org/*", {
	type : "intent"
});

App.accessRule("http://api.mixpanel.com/*");
App.accessRule("https://api.mixpanel.com/*");
App.accessRule("http://mixpanel.com/*");
App.accessRule("https://mixpanel.com/*");
App.accessRule("*.mxpnl.com/*");
App.accessRule("http://cdn.mxpnl.com/");
App.accessRule("https://cdn.mxpnl.com/");

// Set PhoneGap/Cordova preferences
App.setPreference("BackgroundColor", "0xff0000ff");
App.setPreference("HideKeyboardFormAccessoryBar", false);
App.setPreference("KeyboardShrinksView", true);
App.setPreference("Orientation", "portrait");
App.setPreference("DisallowOverscroll", "true");
App.setPreference("EnableViewportScale", "false", "ios");
App.setPreference("StatusBarOverlaysWebView", "true", "ios");
App.setPreference("StatusBarOverlaysWebView", "false", "android");
// App.setPreference("StatusBarBackgroundColor", "#f0f0f0", "android");
// App.setPreference("StatusBarBackgroundColor", "#f0f0f0", "ios");
App.setPreference("StatusBarStyle", "default");
App.setPreference("KeyboardDisplayRequiresUserAction", "false");
App.setPreference("BackupWebStorage", "local", "ios");
App.setPreference("CordovaWebViewEngine", "CDVWKWebViewEngine", "ios");
App.setPreference("SplashMaintainAspectRatio", true, "android"); // without this, android Splashscreens are distorted!
// App.setPreference("SuppressesLongPressGesture", "true", "ios");
// App.setPreference("Suppresses3DTouchGesture", "false", "ios");

// App.setPreference("CordovaWebViewEngine", "CDVUIWebViewEngine", "ios");


// these lines break Android!  see https://github.com/meteor/meteor/issues/6783
//App.setPreference("FadeSplashScreen", "true");
//App.setPreference("FadeSplashScreenDuration", "0.3");

//App.setPreference("SplashScreenDelay", "0");  // seems to do nothing important

console.log("adding GOOGLE_API_KEY_FOR_ANDROID for uk.co.workingedge.phonegap.plugin.launchnavigator");
App.configurePlugin("uk.co.workingedge.phonegap.plugin.launchnavigator", {
	GOOGLE_API_KEY_FOR_ANDROID : "AAAA7XVBzf0:APA91bGBHVZyCNBcpa85bHzDN_K8D3ODnh5UKTjwzN-SD6-VHJt5NfUDVC0MiICi9GfIbbxy44-rVXnP-YdbZ4pJC-B6Cfbadf9i4vIqway1Y7lQFUxWYWsOfBHsNSmOLmSJP2QlkxV-"
});

App.configurePlugin("phonegap-plugin-push", {
	SENDER_ID : 1019874495997
});

//App.configurePlugin("branch-cordova-sdk", {
//	URI_SCHEME : "spoticle",
//	BRANCH_KEY : "key_live_gdd9wjFWcaZYygTfhfTl9ckdsFj8Rx9K"
//});
//
//App.configurePlugin("branch-config", {
//	URI_SCHEME : "spoticle",
//	BRANCH_KEY : "key_live_gdd9wjFWcaZYygTfhfTl9ckdsFj8Rx9K"
//});

//Set up resources such as icons and launch screens.
App.icons({
	// Android
	android_mdpi : "resources/icons/drawable-mdpi-icon.png",
	android_hdpi : "resources/icons/drawable-hdpi-icon.png",
	android_xhdpi : "resources/icons/drawable-xhdpi-icon.png",
	android_xxhdpi : "resources/icons/drawable-xxhdpi-icon.png",
	android_xxxhdpi : "resources/icons/drawable-xxxhdpi-icon.png",

	// iOS
	app_store : "resources/icons/app-store.png", // 1024x1024
	ios_notification : "resources/icons/ios-notification.png", // 20x20
	ios_notification_2x : "resources/icons/ios-notification@2x.png", // 40x40
	ios_notification_3x : "resources/icons/ios-notification@3x.png", // 60x60
	iphone_2x : "resources/icons/icon-60@2x.png",
	iphone_3x : "resources/icons/icon-60@3x.png",
	ipad : "resources/icons/icon-76.png",
	ipad_2x : "resources/icons/icon-76@2x.png",
	ios_settings : "resources/icons/icon-small.png",
	ios_settings_2x : "resources/icons/icon-small@2x.png",
	ios_settings_3x : "resources/icons/icon-small@3x.png",
	ios_spotlight : "resources/icons/icon-40.png",
	ios_spotlight_2x : "resources/icons/icon-40@2x.png",
	iphone_legacy : "", // 57x57
	iphone_legacy_2x : "", // 114x114
	ipad_spotlight_legacy : "", // 50x50
	ipad_spotlight_legacy_2x : "", // 100x100
	ipad_app_legacy : "", // 72x72
	ipad_app_legacy_2x : "", // 144x144
	ipad_pro : "resources/icons/icon-167.png" // 167x167
});

App.launchScreens({
	// Android
	android_mdpi_portrait : "resources/splash/drawable-port-mdpi-screen.png",
	android_mdpi_landscape : "resources/splash/drawable-land-mdpi-screen.png",
	android_hdpi_portrait : "resources/splash/drawable-port-hdpi-screen.png",
	android_hdpi_landscape : "resources/splash/drawable-land-hdpi-screen.png",
	android_xhdpi_portrait : "resources/splash/drawable-port-xhdpi-screen.png",
	android_xhdpi_landscape : "resources/splash/drawable-land-xhdpi-screen.png",
	android_xxhdpi_portrait : "resources/splash/drawable-port-xxhdpi-screen.png",
	android_xxhdpi_landscape : "resources/splash/drawable-land-xxhdpi-screen.png",

	// iOS
	iphone : "", // 320x480
	iphone_2x : "", // 640x490
	iphone5 : "", // 640x1136
	iphone6 : "", // 750x1334
	iphone6p_portrait : "", // 2208x1242
	iphone6p_landscape : "", // 2208x1242
	ipad_portrait : "", // 768x1024
	ipad_portrait_2x : "", // 1536x2048
	ipad_landscape : "", // 1024x768
	ipad_landscape_2x : "", // 2048x1536
});

// start of environment dependent configuration
console.log("///////////////////////////////////");

var config = {
	dev : {
		bundleIdentifier : "com.spoticle.spoticle-dev",
		bundleDisplayName : "APKTest",
		urlScheme : "spoticleDev",
	},
	test : {
		bundleIdentifier : "com.spoticle.spoticle-test",
		bundleDisplayName : "SpoticleTest",
		urlScheme : "spoticleTest",
	},
	prod : {
		bundleIdentifier : "com.spoticle.spoticle",
		bundleDisplayName : "Spoticle",
		urlScheme : "spoticle",
	},
};


var envConfig = config[this.process.env.BUILD_ENVIRONMENT];
if (envConfig) {
	console.log("building for " + this.process.env.BUILD_ENVIRONMENT);

	//for some reason, cc.fovea.cordova.openwith insists on having both Android and iOS
	//vars defined, regardless of the platform.
	App.configurePlugin("cc.fovea.cordova.openwith", {
		ANDROID_MIME_TYPE : "image/*",
		IOS_URL_SCHEME : envConfig.urlScheme,
		IOS_UNIFORM_TYPE_IDENTIFIER : "public.image"
	});

	// This section sets up some basic app metadata,
	// the entire section is optional.
	App.info({
		id : "com.spoticle",
		name : envConfig.bundleDisplayName,
		author : "Spoticle Team",
		email : "team@spoticle.com",
		website : "https://spoticle.com",
		version : version,
		buildNumber : buildNumber
	});

	App.configurePlugin("cordova-plugin-customurlscheme", {
		URL_SCHEME : envConfig.urlScheme
	});
	if (this.process.env.BUILD_PLATFORM == "ios") {
		App.appendToConfig(
			`<platform name="ios">
				<custom-config-file platform="ios" target="*-Info.plist" parent="CFBundleDisplayName">
					<string>` + envConfig.bundleDisplayName + `</string>
				</custom-config-file>
				<custom-config-file platform="ios" target="*-Info.plist" parent="CFBundleIdentifier">
					<string>` + envConfig.bundleIdentifier + `</string>
				</custom-config-file>
				<custom-config-file platform="ios" target="*-Info.plist" parent="UIStatusBarHidden">
					<true/>
				</custom-config-file>
				<custom-config-file platform="ios" target="*-Info.plist" parent="UIViewControllerBasedStatusBarAppearance">
					<false/>
				</custom-config-file>
				<custom-config-file platform="ios" target="*-Info.plist" parent="NSAppTransportSecurity">
			        <dict>
			            <key>NSAllowsArbitraryLoads</key>
			            <true/>
			        </dict>
			    </custom-config-file>
				<custom-preference name="ios-XCBuildConfiguration-PRODUCT_NAME" value="` + envConfig.bundleDisplayName + `" />
				<custom-preference name="ios-XCBuildConfiguration-IPHONEOS_DEPLOYMENT_TARGET" value="10.3" buildType="debug" quote="none" />
    			<custom-preference name="ios-XCBuildConfiguration-IPHONEOS_DEPLOYMENT_TARGET" value="10.3" buildType="release" />
    			<custom-preference name="ios-XCBuildConfiguration-CODE_SIGN_IDENTITY" value="iPhone Developer" buildType="release" />
				<custom-preference name="ios-XCBuildConfiguration-CODE_SIGN_IDENTITY" value="iPhone Developer" buildType="debug" />
				<custom-preference name="ios-XCBuildConfiguration-TARGETED_DEVICE_FAMILY" value="1" buildType="release" />
				<custom-preference name="ios-XCBuildConfiguration-TARGETED_DEVICE_FAMILY" value="1" buildType="debug" />
				<custom-config-file platform="ios" target="*-Info.plist" parent="NSPhotoLibraryUsageDescription">
					<string>Spoticle needs to access the Photo Library to let you upload photos if you choose to.</string>
				</custom-config-file>
				<custom-config-file platform="ios" target="*-Info.plist" parent="NSPhotoLibraryAddUsageDescription">
					<string>Spoticle also stores your original images in your photo library for you.</string>
				</custom-config-file>
				<custom-config-file platform="ios" target="*-Info.plist" parent="NSLocationAlwaysUsageDescription">
					<string>Spoticle tries to deliver relevant content around you.</string>
				</custom-config-file>
				<custom-config-file platform="ios" target="*-Info.plist" parent="NSLocationWhenInUseUsageDescription">
					<string>Spoticle tries to deliver relevant content around you.</string>
				</custom-config-file>
				<custom-config-file platform="ios" target="*-Info.plist" parent="ITSAppUsesNonExemptEncryption">
					<false/>
				</custom-config-file>
				<custom-config-file platform="ios" target="*-Info.plist" parent="UISupportedInterfaceOrientations" mode="replace">
				    <array>
				        <string>UIInterfaceOrientationPortrait</string>
				    </array>
				</custom-config-file>
				<custom-config-file platform="ios" target="*-Info.plist" parent="LSApplicationQueriesSchemes" mode="merge">
				    <array>
				        <string>navigator</string>
				        <string>cflmobile</string>
				    </array>
				</custom-config-file>
				<custom-config-file platform="ios" target="Entitlements-Release.plist" parent="aps-environment" mode="replace">
		        	<string>production</string>
		        </custom-config-file>
		        <custom-config-file platform="ios" target="Entitlements-Debug.plist" parent="aps-environment" mode="replace">
		        	<string>development</string>
		        </custom-config-file>
				<splash src="../../../resources/splash/Default@3x~universal~anyany.png" />
			</platform>`
		);
	}
} else {
	console.log("*** WARNING: Unhandled BUILD_ENVIRONMENT in mobile-config.js: " + this.process.env.BUILD_ENVIRONMENT);
}

// <custom-preference name="ios-XCBuildConfiguration-TARGETED_DEVICE_FAMILY" value="1" buildType="release" />
// <custom-preference name="ios-XCBuildConfiguration-TARGETED_DEVICE_FAMILY" value="1" buildType="debug" />
// <custom-preference name="ios-XCBuildConfiguration-CODE_SIGN_IDENTITY" value="iPhone Developer" buildType="release" />
// <custom-preference name="ios-XCBuildConfiguration-CODE_SIGN_IDENTITY" value="iPhone Developer" buildType="debug" />

//App.appendToConfig(`
//<widget 
//	id="com.spoticle" 
//	version="` + version + `" 
//	xmlns="http://www.w3.org/ns/widgets" 
//	xmlns:cdv="http://cordova.apache.org/ns/1.0" 
//	xmlns:android="http://schemas.android.com/apk/res/android">
//	<!-- We will make some changes in this node later -->
//	<platform name="android">
//		<allow-intent href="market:*" />
//	</platform>
//	<platform name="ios">
//		<allow-intent href="itms:*" />
//		<allow-intent href="itms-apps:*" />
//	</platform>
//</widget>
//`);
//
//App.appendToConfig(`
//<!-- Locate the android platform -->
//<platform name="android">
//	<!-- Add a new intent-filter node to the app activity :	-->
//	<config-file target="AndroidManifest.xml" parent="./application/activity">
//		<intent-filter>
//			<action android:name="android.intent.action.VIEW" />
//			<category android:name="android.intent.category.DEFAULT" />
//			<category android:name="android.intent.category.BROWSABLE" />
//			<data android:scheme="file" />
//			<data android:mimeType="*/*" />
//			<data android:pathPattern=".*\.txt" />
//		</intent-filter>
//	</config-file>
//</platform>
//`);

console.log("///////////////////////////////////");

// if (this.process.env.BUILD_PLATFORM == "ios") {
// 	App.appendToConfig(
// 		`<platform name="ios">

// 	  	</platform>`
// 	);
// <custom-preference name="ios-xcodefunc" func="removeSourceFile">
//     <arg type="String" value="Spoticle/Plugins/cordova-plugin-console/CDVLogger.m" flag="path" />
// </custom-preference>
//} else if (this.process.env.BUILD_PLATFORM == "android") {
//
//	App.appendToConfig(
//		`<config-file target="AndroidManifest.xml" parent="/*" mode="merge">
//			<uses-permission android:name="android.permission.CAMERA" />
//			<uses-feature android:name="android.hardware.camera" />
//			<uses-feature android:name="android.hardware.camera.autofocus" />
//		</config-file>`
//	);

// }



// <custom-resource type="image" src="../../../resources/icons/ios-marketing.png" catalog="AppIcon.appiconset" scale="1x" idiom="ios-marketing" />
